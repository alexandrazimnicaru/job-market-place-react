import React from 'react';
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';

import './Profile.css';

const Profile = () => (
  <Card className="job-card">
    <CardTitle title="Profile" />

    <CardText>
        <TextField hintText="First name" fullWidth={true} /><br />

        <TextField hintText="Last name" fullWidth={true} /><br />

        <TextField hintText="Email" value="wouter@l1nda.nl" fullWidth={true} /><br />

        <TextField hintText="Telephone" fullWidth={true} /><br />

        <DatePicker hintText="Date of birth" 
            mode="landscape"
            openToYearSelection={true} 
            fullWidth={true} />
    </CardText>

    <CardActions className="job-card__actions">
      <RaisedButton label="Save" secondary={true} />
    </CardActions>
  </Card>
);

export default Profile;
