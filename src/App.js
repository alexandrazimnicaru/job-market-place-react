import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import CardTravel from 'material-ui/svg-icons/action/card-travel';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';

import Jobs from './Jobs/Jobs';

import {purple800, tealA400} from 'material-ui/styles/colors';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: purple800,
    pickerHeaderColor: tealA400,
    accent1Color: tealA400
  }
});

const App = () => (
  <MuiThemeProvider muiTheme={muiTheme}>
    <section>
      <AppBar
        title="Job marketplace" 
        iconElementLeft={<IconButton><CardTravel color={tealA400}/></IconButton>}
        iconElementRight={<IconButton><AccountCircle color={tealA400}/></IconButton>}/>
        
      <Jobs />
    </section>
  </MuiThemeProvider>
);

export default App;
