import React from 'react';
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';

import './Job.css';

const Job = () => (
  <Card className="job-card">
    <CardTitle title="Job title" subtitle="The company offering the job" />

    <CardText>
        <section>
            <h4>Description</h4>
            <p>
                The job description: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
            </p>
        </section>

        <section>
            <h4>Responsibilities</h4>
            <ul>
                <li>Show on time</li>
                <li>Do your job</li>
            </ul>
        </section>

        <section>
            <h4>Requirements</h4>
            <ul>
                <li>Great skills</li>
                <li>No mood swings</li>
            </ul>
        </section>

        <section>
            <h4>Benefits</h4>
            <ul>
                <li>A salary</li>
                <li>Free public transport</li>
            </ul>
        </section>

        <section>
            <h4>Work hours</h4>
            <p>32 hours a week</p>
        </section>
    </CardText>

    <CardActions className="job-card__actions">
      <RaisedButton label="Apply" secondary={true} />
    </CardActions>
  </Card>
);

export default Job;
