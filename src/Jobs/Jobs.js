import React from 'react';
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';

import './Jobs.css';

const Jobs = () => (
  <Card className="job-card">
    <CardTitle title="Job title" subtitle="The company offering the job" />

    <CardText>
      The job summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
      Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
      Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
    </CardText>
    
    <CardActions className="job-card__actions">
      <RaisedButton label="View" secondary={true} />
    </CardActions>
  </Card>
);

export default Jobs;
