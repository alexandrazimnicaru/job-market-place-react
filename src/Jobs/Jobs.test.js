import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import '../../setupTests';

import Jobs from './Jobs';

it('renders without crashing', () => {
    shallow(<Jobs />);
});

it('renders job cards', () => {
    const wrapper = shallow(<Jobs />);

    expect(wrapper.find('.job-card').exists()).toEqual(true);
});