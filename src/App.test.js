import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import '../setupTests';

import App from './App';
import Jobs from './Jobs/Jobs';

it('renders without crashing', () => {
    shallow(<App />);
});
